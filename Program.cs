﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Threading.Tasks;
using Conrad.Commands;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Newtonsoft.Json;
using UniqueIdGenerator.Net;
using Microsoft.EntityFrameworkCore;

namespace Conrad
{
    class Program
    {
        static void Main(string[] args) => new Program().Run().GetAwaiter().GetResult();

        private DiscordSocketClient _client;
        private SocketSelfUser _user;
        private CommandService _commands;

        public async Task Run()
        {
            General.R = new Random();

            General.Snowflake = new Generator(4, new DateTime(2017, 10, 18, 8, 35, 0));

            Poll.OptionTitles.Add(":one:", "Option 1");
            Poll.OptionTitles.Add(":two:", "Option 2");
            Poll.OptionTitles.Add(":three:", "Option 3");
            Poll.OptionTitles.Add(":four:", "Option 4");
            Poll.OptionTitles.Add(":five:", "Option 5");
            Poll.OptionTitles.Add(":six:", "Option 6");
            Poll.OptionTitles.Add(":seven:", "Option 7");
            Poll.OptionTitles.Add(":eight:", "Option 8");
            Poll.OptionTitles.Add(":nine:", "Option 9");
            Poll.OptionTitles.Add(":keycap_ten:", "Option 10");
            Poll.OptionTitles.Add(":put_litter_in_its_place:", "None");

            _client = new DiscordSocketClient();
            _commands = new CommandService();

            General.Config = new BotConfig();

            General.Store = new ConfigStore("./conf.json");
            General.Config = General.Store.Load();

            Console.WriteLine("I am configured");

            #pragma warning disable CS1998
            _client.Connected += async () => { Console.WriteLine("I am connected"); };
            #pragma warning restore CS1998

            _client.Ready += async () =>
            {
                await _client.SetGameAsync(General.Config.Status);

                _user = _client.CurrentUser;

                Console.WriteLine("I am ready");

                await InitDatabase();
            };

            await InitCommands();

            await _client.LoginAsync(TokenType.Bot, General.Config.LoginToken);
            await _client.StartAsync();

            Console.ReadKey();

            General.Store.Save(General.Config);

            await _client.LogoutAsync();
            await _client.StopAsync();
        }

        private async Task InitDatabase() {
            General.DB = new ConradContext();

            General.DB.Database.EnsureCreated();

            foreach (var a in General.DB.Polls) await a.LoadAsync(_client);
            //if (!General.DB.Database.EnsureCreated()) throw new Exception("Database broken");
        } 

        private async Task InitCommands()
        {
            _client.ReactionAdded += UpdatePollMessages;
            _client.ReactionRemoved += UpdatePollMessages;
            _client.MessageReceived += HandleCommand;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private async Task UpdatePollMessages(Cacheable<IUserMessage, ulong> msg, ISocketMessageChannel channel, SocketReaction r)
        {
            var m = await msg.GetOrDownloadAsync();

            if (General.DB.Polls.Any(e => e.MessageId == m.Id.ToString()))
            {
                General.DB.Polls.Where(e => e.MessageId == m.Id.ToString()).FirstOrDefault().Message = m;
            }
        }

        private async Task HandleCommand(IMessage msg)
        {
            var m = msg as IUserMessage;

            if (!m.Author.IsBot)
            {
                int argPos = 0;
                if (m.Content != "" + General.Config.CommandPrefix && m.Content[1] != General.Config.CommandPrefix && (m.HasCharPrefix(General.Config.CommandPrefix, ref argPos)))
                {
                    await msg.Channel.TriggerTypingAsync();

                    var result = await _commands.ExecuteAsync(new CommandContext(_client, m), argPos);
                    if (!result.IsSuccess)
                        await msg.Channel.SendMessageAsync(result.ErrorReason);
                }
            }
        }
    }

    static class General {

        public static Random R { get; set; }
        public static ConfigStore Store { get; set; }
        public static BotConfig Config { get; set; }
        public static List<Poll> P { get; } = new List<Poll>();
        public static Generator Snowflake { get; set; }
        public static ConradContext DB { get; set; }

    }

    public class ConradContext : DbContext
    {
        public DbSet<Poll> Polls { get; set; }
        public DbSet<Result> Results { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder o)
        {
            o.UseMySQL(General.Config.SqlConnect);
        }

        protected override void OnModelCreating(ModelBuilder m)
        {
            base.OnModelCreating(m);

            m.Entity<Poll>(e =>
            {
                e.HasKey(p => p.Id);
                e.Property(p => p.Id).ValueGeneratedNever();
                e.Ignore(p => p.Channel);
                e.Ignore(p => p.Server);
                e.Ignore(p => p.Message);
                e.Ignore(p => p.User);
                e.Ignore(p => p.Timer);
                e.Ignore(p => p.Reminders);
                e.Ignore(p => p.Emotes);
            });
            m.Entity<Result>(e =>
            {
                e.HasKey(p => p.Id);
                e.Property(p => p.Id).ValueGeneratedNever();
            });
        }
    }

    public sealed class ConfigStore
    {
        private readonly string _jsonPath;

        public ConfigStore(string path)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!File.Exists(path)) File.WriteAllText(path, "{}");

            _jsonPath = path;
        }

        public BotConfig Load()
        {
            return JsonConvert.DeserializeObject<BotConfig>(File.ReadAllText(_jsonPath));
        }

        public void Save(BotConfig config)
        {
            File.WriteAllText(_jsonPath, JsonConvert.SerializeObject(config, Formatting.Indented));
        }
    }

    public class BotConfig
    {
        public string LoginToken { get; set; }

        public ulong OwnerId { get; set; }

        public int PinThreshold { get; set; } = 3;

        public char CommandPrefix { get; set; }
        
        public uint EmbedColor { get; set; } = 0xd21e1e;

        public string Status { get; set; }

        public string SqlConnect { get; set; }
    }
}
