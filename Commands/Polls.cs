using Discord;
using Discord.Commands;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Discord.WebSocket;
using EO = EmojiOne.Core.EmojiOne;

namespace Conrad.Commands
{
    public class PollCommands : ModuleBase
    {
        [Command("poll"), Summary("Create a poll.")]
        public async Task PollC(string type, string title, string description, string time, int options = 0)
        {
            if (Context.Guild != null) {
                List<Emoji> e;
                List<string> j;
                EmbedBuilder b = Embed(title, description);
                var c = Context.Channel as IGuildChannel;
                
                switch (type)
                {
                    case "proposal":
                        e = Proposal();
                        b.AddInlineField("✅", "Approve");
                        b.AddInlineField("❎", "Disapprove");
                        break;
                    case "option":
                        j = Option(options, out e);
                        var i = 0;
                        foreach (var w in e) {
                            if (j != null) {
                                b.AddInlineField(w.Name, j[i]);
                            }
                            i++;
                        }
                        break;
                    default:
                        throw new Exception("Not a valid poll type!");
                }
                
                if (DateTime.TryParse(time, out var r)) {
                    b.WithFooter("Ends " + r);

                    var m = await Context.Channel.SendMessageAsync("", false, b);
                    await m.PinAsync();
                    General.DB.Polls.Add(new Poll(title, r, c, m, Context.User, e));
                    try { await General.DB.SaveChangesAsync(); } catch (Exception ex) { await Context.Channel.SendMessageAsync(ex.Message + "\n" + ex.InnerException.Message); }
                } else { throw new Exception("Not a time"); }
            }
        }

        public EmbedBuilder Embed(string title, string description)
        {
            try
            {
                var b = new EmbedBuilder{
                    Color = new Color(General.Config.EmbedColor)
                };

                var au = new EmbedAuthorBuilder{
                    Name = Context.Message.Content,
                    IconUrl = Context.User.GetAvatarUrl()
                };
                b.WithAuthor(au);

                b.WithTitle(title);
                b.WithDescription(description);

                return b;
            }
            catch
            {
                throw;
            }
        }

        public List<Emoji> Proposal()
        {
            var e = new List<Emoji>();
            e.Add(new Emoji(":white_check_mark:")); e.Add(new Emoji(":negative_squared_cross_mark:"));

            return e;
        }

        public List<string> Option(int options, out List<Emoji> e) {
            e = new List<Emoji>();
            var j = new List<string>();
            var r = new string[] { ":one:", ":two:", ":three:", ":four:", ":five:", ":six:", ":seven:", ":eight:", ":nine:", ":keycap_ten:" };
            var i = 0;
            while (i < options) {
                e.Add(new Emoji(r[i]));
                j.Add("Option " + (i + 1));

                i++;
            }
            e.Add(new Emoji(":put_litter_in_its_place:"));
            j.Add("None");

            return j;
        }
    }

    public class Poll
    {
        public static Dictionary<string, string> OptionTitles { get; } = new Dictionary<string, string>();

        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime End { get; set; }
        public string ChannelId { get; set; }
        public IGuildChannel Channel { get; set; }
        public string ServerId { get; set; }
        public IGuild Server { get; set; }
        public string MessageId { get; set; }
        public IUserMessage Message { get; set; }
        public string UserId { get; set; }
        public IUser User { get; set; }
        public string EmoteIds { get; set; }
        public List<Emoji> Emotes { get; set; }
        public Timer Timer { get; set; }
        public Dictionary<double, Timer> Reminders { get; } = new Dictionary<double, Timer>();

        private Poll() {}
        public Poll(string t, DateTime r, IGuildChannel c, IUserMessage m, IUser u, List<Emoji> e)
        {
            Id = General.Snowflake.Next();
            Name = t;
            End = r;
            Channel = c;
            ChannelId = Channel.Id.ToString();
            Server = c.Guild;
            ServerId = Server.Id.ToString();
            Message = m;
            MessageId = Message.Id.ToString();
            User = u;
            UserId = User.Id.ToString();
            Emotes = e;
            EmoteIds = "";
            foreach (var a in Emotes) EmoteIds += (a.Name + " ");
            DateTime n = DateTime.Now;
            TimeSpan left = End - n;
            var re = new Dictionary<double, TimeSpan>();
            re.Add(48, End - new TimeSpan(2, 0, 0, 0) - n);
            re.Add(24, End - new TimeSpan(1, 0, 0, 0) - n);
            re.Add(6, End - new TimeSpan(6, 0, 0) - n);
            re.Add(2, End - new TimeSpan(2, 0, 0) - n);
            re.Add(0.5, End - new TimeSpan(0, 30, 0) - n);
            re.Add(0.25, End - new TimeSpan(0, 15, 0) - n);

            if (left < TimeSpan.Zero)
            {
                throw new Exception("Poll ends before the current time!");
            }
            foreach (var s in re) {
                if (s.Value > TimeSpan.Zero) {
                    Reminders.Add(s.Key, new Timer(async x => { await RemindAsync(s.Key); }, null, s.Value, Timeout.InfiniteTimeSpan));
                }
            }
            Timer = new Timer(async x => { await ResultsAsync(); }, null, left, Timeout.InfiniteTimeSpan);
        }

        public async Task LoadAsync(DiscordSocketClient cli)
        {
            var cid = UInt64.Parse(ChannelId);
            var mid = UInt64.Parse(MessageId);
            var uid = UInt64.Parse(UserId);
            Channel = cli.GetChannel(cid) as IGuildChannel;
            Message = await (Channel as IMessageChannel).GetMessageAsync(mid) as IUserMessage;
            User = cli.GetUser(uid);
            Emotes = new List<Emoji>();
            foreach (var a in EmoteIds.Split(' ')) Emotes.Add(new Emoji(a));;

            DateTime n = DateTime.Now;
            TimeSpan left = End - n;
            var re = new Dictionary<double, TimeSpan>();
            re.Add(48, End - new TimeSpan(2, 0, 0, 0) - n);
            re.Add(24, End - new TimeSpan(1, 0, 0, 0) - n);
            re.Add(6, End - new TimeSpan(6, 0, 0) - n);
            re.Add(2, End - new TimeSpan(2, 0, 0) - n);
            re.Add(0.5, End - new TimeSpan(0, 30, 0) - n);
            re.Add(0.25, End - new TimeSpan(0, 15, 0) - n);

            if (left < TimeSpan.Zero)
            {
                if (!General.DB.Results.Any(r => r.Id == this.Id))
                    await ResultsAsync();
                return;
            }
            foreach (var s in re) {
                if (s.Value > TimeSpan.Zero) {
                    Reminders.Add(s.Key, new Timer(async x => { await RemindAsync(s.Key); }, null, s.Value, Timeout.InfiniteTimeSpan));
                }
            }
            Timer = new Timer(async x => { await ResultsAsync(); }, null, left, Timeout.InfiniteTimeSpan);
        }

        private async Task RemindAsync(double hours)
        {
            await Message.Channel.SendMessageAsync("Poll " + Format.Bold(Name) + " will end in " + hours + " hours!");
        }

        private async Task ResultsAsync()
        {
            try
            {
                await Message.AddReactionAsync(new Emoji("🆗"));

                var b = new EmbedBuilder{
                    Color = new Color(General.Config.EmbedColor),
                    Title = "\"" + Name + "\" ended"
                };

                var au = new EmbedAuthorBuilder{
                    Name = "Poll from " + User.Username,
                    IconUrl = User.GetAvatarUrl()
                };
                b.WithAuthor(au);
                var t = 0;
                var r = new Dictionary<IEmote, int>();
                var z = new Dictionary<ulong, IEmote>();
                var p = new Dictionary<IEmote, double>();

                foreach (var a in Message.Reactions)
                {
                    if (Emotes.Count == 0 || Emotes.Any(e => (e.Name == a.Key.Name) || (e.Name == EO.ShortnameToUnicode(a.Key.Name)) || (e.Name == EO.ToShort(a.Key.Name))))
                    {
                        string e = (a.Key is Emote i ? $"{i.Name}:{i.Id}" : a.Key.Name);

                        var u = await Message.GetReactionUsersAsync(e);
                        foreach (var y in u)
                        {
                            if (!z.ContainsKey(y.Id))
                            {
                                z.Add(y.Id, a.Key);

                                if (!r.ContainsKey(a.Key))
                                {
                                    r.Add(a.Key, 1);
                                }
                                else
                                {
                                    r[a.Key]++;
                                }
                            }
                        }
                    }
                }

                t += z.Count;

                var v = new List<int>();
                foreach (var a in Emotes) {
                    if (r.Any(m => m.Key.Name == EO.ShortnameToUnicode(a.Name))) {
                        v.Add(r.Where(m => m.Key.Name == EO.ShortnameToUnicode(a.Name)).FirstOrDefault().Value);
                        p.Add(a, r.Where(m => m.Key.Name == EO.ShortnameToUnicode(a.Name)).FirstOrDefault().Value / (double)t);

                        b.AddInlineField(a.Name, String.Format("{0:0.#%} (" + r[r.Where(m => m.Key.Name == EO.ShortnameToUnicode(a.Name)).FirstOrDefault().Key] + ")", p[a]));
                    }
                    else {
                        v.Add(0);
                        p.Add(a, 0);
                        r.Add(a, 0);

                        b.AddInlineField(a.Name, String.Format("{0:0.#%} (" + 0 + ")", 0));
                    }
                }

                General.DB.Results.Add(new Result(Id, Emotes, v));
                await General.DB.SaveChangesAsync();

                await Message.Channel.SendMessageAsync("", false, b);

                await Message.UnpinAsync();
            }
            catch { throw; }
        }
    }

    public class Result
    {
        public string Id { get; set; }
        public string Emojis { get; set; }
        public string Votes { get; set; }

        public Result() {}
        
        public Result(string i, List<Emoji> e, List<int> v) {
            Id = i;
            Emojis = "";
            foreach (var a in e) Emojis += (a.Name + " ");
            Votes = "";
            foreach (var a in v) Votes += (a + " ");
        }
    }
}