using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Conrad.Commands
{
    public class Help : ModuleBase
    {
        private CommandService _cs;
        public Help(CommandService cs)
        {
            _cs = cs;
        }

        [Command("help"), Summary("List commands and get help with usage.")]
        public async Task HelpC(string command = "")
        {
            var b = new EmbedBuilder();
            b.WithColor(new Color(General.Config.EmbedColor));

            var au = new EmbedAuthorBuilder{
                Name = Context.Message.Content,
                IconUrl = Context.User.GetAvatarUrl()
            };
            b.WithAuthor(au);

            if (command == "")
            {
                b.WithTitle("Conrad Commands");

                foreach (var w in _cs.Commands) {
                    var f = new EmbedFieldBuilder{
                        Name = Format.Code(w.Name),
                        Value = w.Summary
                    };
                    f.IsInline = true;

                    b.Fields.Add(f);
                }

                await Context.Channel.SendMessageAsync("", false, b.Build());
            }
            else
            {
                
                var exists = false;

                foreach (var w in _cs.Commands)
                {
                    if (w.Aliases.Contains(command))
                    {
                        exists = true;

                        var cn = w.Name;

                        foreach (var p in w.Parameters)
                        {
                            cn += " <" + p.Name + ">";
                        }

                        b.WithTitle(Format.Code(cn));
                        b.WithDescription(w.Summary);

                        if (w.Aliases.Count > 1)
                        {
                            var az = "";
                            var i = 0;

                            foreach (var a in w.Aliases)
                            {
                                if (i != 0)
                                    az += a + ", ";
                                i++;
                            }

                            var f = new EmbedFooterBuilder{
                                Text = "Aliases: " + az.Substring(0, az.Length - 2)
                            };
                            b.WithFooter(f);
                        }
                    }
                }
                if (!exists) {
                    b.WithTitle("Uh oh!");
                    b.WithDescription("That command doesn't exist!");
                }

                await Context.Channel.SendMessageAsync("", false, b.Build());
            }
        }
    }
}
